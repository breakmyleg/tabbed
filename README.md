### Preview

![gif](https://i.postimg.cc/tJpQQWfY/tabbed.gif "gif")


### Installation

Requirements : git, Xlib\

- git clone https://gitlab.com/rmnsa/tabbed.git
- cd tabbed
- sudo make install clean


### Keybinds

|function|key1|key2|key3|
|:------:|:--:|:--:|:--:|
|new tab|Alt|Shift|Enter|
|close tab|Alt|q|
|rotate tabs|Alt|Tab|
|full-screen|F11|

### Basic use

- man tabbed

##### TL;DR

- tabbed [name of program]
- tabbed -r [number of arguments] [program] [argument to program]
